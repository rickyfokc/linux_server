#!/bin/bash 
###########################################################################
# 
#   1. Update YUM repository 
#   2. Install epel-release repository
#   3. Install firewalld
#   4. Enable firewalld
#   5. Install GIT
#
###########################################################################

# ------------------------------ #
# Define Variables
password='!234Qwer'
# ------------------------------ #

# 1. Update YUM repository 
echo "$password" | sudo -S yum update -y 

# 2. Install epel-release repository
echo "$password" | sudo -S yum install epel-release -y

# 3. Install firewalld
echo "$password" | sudo -S install firewalld -y

# 4. Enable firewalld
echo "$password" | sudo -S systemctl enable firewalld
echo "$password" | sudo -S systemctl start firewalld
# if error
echo "$password" | sudo -S systemctl restart dbus

# 5. Install GIT
echo "$password" | sudo -S yum install git -y
