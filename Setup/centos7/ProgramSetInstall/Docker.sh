#!/bin/bash 
###########################################################################
# 
#   Installation of Docker
#   1. Install required dependencies
#   2. Add the Docker stable repository
#   3. Install Docker-ce
#   4. Start the Docker daemon and enable it to automatically start at boot time
#
#   Reference: https://linuxize.com/post/how-to-install-and-use-docker-on-centos-7/
#              https://www.cyberciti.biz/faq/install-use-setup-docker-on-rhel7-centos7-linux/
#              http://www.kean.ph/install-docker-on-openvz-vps/
#              https://thegeeksalive.com/how-to-setup-openvz-7-on-centos-7/
###########################################################################

# 1. Install required dependencies
sudo yum install docker
sudo yum remove docker docker-common docker-selinux docker-engine
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce
# 2. Add the Docker stable repository
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# 3. Install Docker-ce
yum -y install docker-ce

# 4. Start the Docker daemon and enable it to automatically start at boot time
systemctl start docker
systemctl enable docker