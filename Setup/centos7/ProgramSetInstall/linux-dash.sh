#!/bin/bash 
###########################################################################
# 
#   Installation of Web-Vmstat
#   1. Clone linux-dash from GitHub  to Apache public folder called ‘linux-dash‘ (i.e. /var/www or /var/www/html)
#   2. Open Port 80 for network access
#   3. Password Protect linux-dash
#
#   Reference: https://www.tecmint.com/monitors-linux-server-performance-remotely-using-web-browser/
###########################################################################

# !!-----Please Install NodeJs and PM2 First from nodeJs.sh-----!!

# ------------------------------ #
# Define Variables
password='!234Qwer'
# ------------------------------ #

# 1. Clone linux-dash from GitHub
echo "$password" | sudo -S git clone --depth 1 https://github.com/afaqurk/linux-dash.git

# 2. Start the application
cd linux-dash/app/server
echo "$password" | sudo -S npm install --production
echo "$password" | sudo -S pm2 start index.js

# 3. Open Port 80 for network access
echo "$password" | sudo -S systemctl start firewalld
echo "$password" | sudo -S firewall-cmd --zone=public --add-port=80/tcp --permanent
echo "$password" | sudo -S firewall-cmd --reload
