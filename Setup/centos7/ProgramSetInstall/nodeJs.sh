#!/bin/bash 
###########################################################################
# 
#   Installation of NodeJs
#   1. Upate NodeJs In YUM Repo
#   2. Install NodeJs
#   3. Install PM2
#
###########################################################################
# ------------------------------ #
# Define Variables
password='!234Qwer'
# ------------------------------ #

# 1. Upate NodeJs In YUM Repo
echo "$password" | sudo -S  curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash -

# 2. Install NodeJs
echo "$password" | sudo -S  yum install -y nodejs

# 3. Install PM2
echo "$password" | sudo -S  npm install pm2 -g
