#!/bin/bash 
###########################################################################
# 
#   Installation of MongoDB
#   1. Install MongoDB
#
###########################################################################

# 1. Install MongoDB
# ----------------------------------------------------------------------------------------------------
# Define Variable and Write to /etc/yum.repos.d/mongodb.repo 
MONGODBREPOTEXT="[mongodb-org-4.0]\
\nname=MongoDB Repository\
\nbaseurl=https://repo.mongodb.org/yum/redhat/\$releasever/mongodb-org/4.0/x86_64/\
\ngpgcheck=1\
\nenabled=1\
\ngpgkey=https://www.mongodb.org/static/pgp/server-4.0.asc"
# Create MongoDB Repo File ...
echo -e $MONGODBREPOTEXT >> /etc/yum.repos.d/mongodb-org.repo
# Starting Mongodb Installation
yum -y install mongodb-org
# Set the mongoDB can be access by internet
sed -i 's/bindIp: 127.0.0.1/#bindIp: 127.0.0.1/g' /etc/mongod.conf
# Reload Mongodb After Setup
systemctl reload mongod
# Open 27017 port in firewalld
firewall-cmd --zone=public --add-port=80/tcp --permanent
# Reload Firewalld after setup
firewall-cmd --reload