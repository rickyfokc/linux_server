#!/bin/bash 
###########################################################################
# 
#   1. Change Password On Current Account
#   2. Create another login account
#   3. Change Password for login account
#   4. Grant administrator privileges to login account [Optional]
#      Use "sudo" to execute command (e.g sudo ls -la /root)
#   5. Restrict SSH login to root account
#
###########################################################################

# ------------------------------ #
# Define Variables
password='!234Qwer'
# ------------------------------ #

# 1. Change Password On Current Account
echo -e "$password\n$password" | passwd

# 2. Create another login account
/usr/sbin/adduser rickyfok

# 3. Change Password for login account
echo -e "$password\n$password" | passwd rickyfok

# 4. Grant administrator privileges to login account [Optional]
#    Install sudo mode if not be installed
yum install -y sudo
usermod -aG wheel rickyfok

# 5. Restrict SSH login to root account
sed -i 's/#PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config 
# Reload the /etc/ssh/sshd_config to disable Root Login
systemctl restart sshd.service

