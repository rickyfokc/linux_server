#!/bin/bash 
###########################################################################
# 
#   1. updates the list of available packages and their versions
#   2. installs newer versions of the packages
#
###########################################################################
# ------------------------------ #
# Define Variables
password='!234Qwer'
# ------------------------------ #
# 1. updates the list of available packages and their versions
echo "$password" | sudo -S sudo apt-get update;

# 2. installs newer versions of the packages
echo "$password" | sudo -S sudo apt-get upgrade -y;
